const ROUTES = {
  home: '/pokedex',
  pokemon: '/pokemons/detail'
}

export default ROUTES
