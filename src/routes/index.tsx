import { FunctionComponent } from 'react'
import { BrowserRouter, Routes, Route, Navigate } from 'react-router-dom'
import Pokedex from '../pages/Pokedex'
import PokemonDetail from '../pages/PokemonDetail'
import PokemonNotFound from '../pages/PokemonNotFound/index'

export const AppRoutes: FunctionComponent = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Navigate to="/pokedex" replace />}/>
        <Route path="/pokedex" element={<Pokedex />} />
        <Route path="/pokemon/:pokemonName/detail" element={<PokemonDetail />} />
        <Route path="*" element={<PokemonNotFound />} />
      </Routes>
  </BrowserRouter>
  )
}
