import { configureStore, ThunkAction, Action, combineReducers } from '@reduxjs/toolkit'
import pokemonsReducer from '../features/pokemon/pokemonSlice'
import thunk from 'redux-thunk'

const rootReducer = combineReducers({
  pokemons: pokemonsReducer
})

export const store = configureStore({
  reducer: rootReducer,
  middleware: [thunk]
})

export type AppDispatch = typeof store.dispatch
export type RootState = ReturnType<typeof rootReducer>
export type AppThunk<ReturnType = void> = ThunkAction<
ReturnType,
RootState,
unknown,
Action<string>
>
