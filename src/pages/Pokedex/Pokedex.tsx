import { FunctionComponent, useEffect, useState } from 'react'
import { Grid, Typography, Box } from '@mui/material'
import { useAppSelector, useAppDispatch } from '../../app/hooks'
import { fetchPokemonList, selectAllPokemonsList, selectPokemonListStatus } from '../../features/pokemon/pokemonSlice'
import PokeCard from './components/PokeCard'
import PaginationSearch from './components/PaginationSearch'
import loadingPikachu from '../../assets/images/pikachuRunning.gif'

const Pokedex: FunctionComponent = () => {
  const dispatch = useAppDispatch()
  const pokemonList = useAppSelector(selectAllPokemonsList)
  const pokemonsStatus = useAppSelector(selectPokemonListStatus)
  const [displayCount, setDisplayCount] = useState(25)
  const [displayedPage, setDisplayedPage] = useState(1)
  const fromPokemon = ((displayedPage - 1) * displayCount)

  useEffect(() => {
    if (pokemonsStatus === 'idle') {
      // @ts-expect-error
      void dispatch(fetchPokemonList())
    }
  }, [pokemonsStatus, dispatch])

  return (
    <>
      {pokemonList.length === 0
        ? (
          <Box sx={{
            height: '100%',
            width: '100%',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            scale: '40%'
          }}>
            <img src={loadingPikachu}></img>
          </Box>
          )
        : (
          <>
            <Typography align="center" variant="h2" sx={{ paddingTop: '1rem' }}>
              POKEDEX
            </Typography>
            <PaginationSearch
              pokemonListLength={pokemonList.length}
              displayCount={displayCount}
              displayedPage={displayedPage}
              setDisplayedPage={setDisplayedPage}
              setDisplayCount={setDisplayCount}
            />
            <Box sx={{
              height: '80%',
              overflowY: 'auto',
              '::-webkit-scrollbar': `{
                width: 10px;
              }`
            }}>
              <Grid container spacing={2} justifyContent="center" alignItems="flex-start">
                {pokemonList.slice(fromPokemon, fromPokemon + displayCount).map((pokemon, index) => (
                  <Grid item key={index}>
                    <PokeCard pokemon={pokemon} />
                  </Grid>
                ))}
              </Grid>
            </Box>
          </>
          )
      }
    </>
  )
}

export default Pokedex
