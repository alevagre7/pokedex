import styled from '@emotion/styled'
import { keyframes } from '@emotion/react'
import { FunctionComponent, useEffect } from 'react'
import { NamedAPIResource } from 'pokenode-ts'
import { Typography } from '@mui/material'
import missingNoSprite from '../../../assets/images/MissingNo.png'
import pokeBallSprite from '../../../assets/images/pokeBallSprite.png'
import { useAppDispatch, useAppSelector } from '../../../app/hooks'
import { fetchPokemonByName, selectPokemonByName } from '../../../features/pokemon/pokemonSlice'
import { useNavigate } from 'react-router-dom'

export interface PokeCardProps {
  pokemon: NamedAPIResource
}

const PokeCardContainer = styled.div`
  width: 7rem;
  height: 9rem;
  border-radius: .375rem;
  background-color: #dbeafe80;
  padding-top: .5rem;
  padding-bottom: .5rem;
  cursor: pointer;
`

const PokeCardName = styled.div`
  color: rgb(51 65 85);
  height: 20%
`

const PokeCardSprite = styled.img`
  width: 100%;
  height: 80%;
  object-fit: contain;
`
const bounceAnimation = keyframes`
  from, 20%, 53%, 80%, to {
    transform: translate3d(0,0,0);
  }

  40%, 43% {
    transform: translate3d(0, -30px, 0);
  }

  70% {
    transform: translate3d(0, -15px, 0);
  }

  90% {
    transform: translate3d(0,-4px,0);
  }
`

const PokeCardSkeleton = styled.img`
width: 100%;
  height: 80%;
  object-fit: contain;
  scale: 25%;
  filter: grayscale(0.4);
  animation: ${bounceAnimation} 1.5s ease-in-out 0.5s infinite
`

const PokeCard: FunctionComponent<PokeCardProps> = ({ pokemon }) => {
  const dispatch = useAppDispatch()
  const navigate = useNavigate()
  const pokemonInfo = useAppSelector((state) => selectPokemonByName(state, pokemon.name))
  const pokemonSprite = pokemonInfo?.sprites?.front_default !== null
    ? pokemonInfo?.sprites?.front_default
    : missingNoSprite

  useEffect(() => {
    if (pokemonInfo === undefined && pokemon?.name !== undefined) {
      // @ts-expect-error
      dispatch(fetchPokemonByName(pokemon.name))
    }
  }, [dispatch, pokemon])

  return (
    <PokeCardContainer onClick={() => navigate(`/pokemon/${pokemon.name}/detail`)}>
      <PokeCardName>
        <Typography variant='h6' align='center'>
          {pokemon.name}
        </Typography>
      </PokeCardName>
        {pokemonInfo === undefined
          ? <PokeCardSkeleton src={pokeBallSprite} />
          : <PokeCardSprite src={pokemonSprite}/>
        }
    </PokeCardContainer>
  )
}

export default PokeCard
