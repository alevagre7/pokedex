import { FunctionComponent, useEffect, useState } from 'react'
import { Pagination, Button, ButtonGroup, Box, Autocomplete, TextField, Typography } from '@mui/material'
import { useAppSelector } from '../../../app/hooks'
import { selectAllPokemonsList } from '../../../features/pokemon/pokemonSlice'
import { useNavigate } from 'react-router-dom'
import ROUTES from '../../../routes/routes'

export interface PaginationSearchProps {
  pokemonListLength: number
  displayCount: number
  displayedPage: number
  setDisplayedPage: React.Dispatch<React.SetStateAction<number>>
  setDisplayCount: React.Dispatch<React.SetStateAction<number>>
}

const PaginationSearch: FunctionComponent<PaginationSearchProps> = (
  { pokemonListLength, displayCount, displayedPage, setDisplayedPage, setDisplayCount }
) => {
  const [pagesCount, setPagesCount] = useState(0)
  const [pokemonNames, setPokemonNames] = useState<string[]>([])
  const pokemonList = useAppSelector(selectAllPokemonsList)
  const navigate = useNavigate()

  useEffect(() => {
    const newPagesCount = pokemonListLength / displayCount
    setPagesCount(Math.trunc((pokemonListLength % displayCount) !== 0 ? newPagesCount + 1 : newPagesCount))
  }, [pokemonListLength, displayCount])

  useEffect(() => {
    return setPokemonNames(pokemonList.map((pokemon) => pokemon.name))
  }, [pokemonList])

  return (
    <Box>
      <Box sx={{
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%'
      }}>
        <Autocomplete
          disablePortal
          id="combo-box-demo"
          options={pokemonNames}
          sx={{ width: '30%', height: '80%' }}
          renderInput={(params) => <TextField {...params} label="Pokemon" size="small" />}
          onChange={(event: any, pokemonName: string | null) => {
            if (pokemonName !== null) navigate(`/pokemon/${pokemonName}/detail`)
          }}
        />
      </Box>
      <Box sx={{
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingBottom: '0.5rem',
        width: '100%'
      }}>
        <Pagination
          count={pagesCount}
          defaultPage={1}
          page={displayedPage}
          onChange={(_e, page) => setDisplayedPage(page)}
        />
        <ButtonGroup variant="text" aria-label="text button group">
          <Typography variant="button" sx={{
            display: 'inline-flex',
            alignItems: 'center',
            fontSize: '1rem',
            marginRight: '1rem'
          }}>
            Display Count
          </Typography>
          <Button sx={{ backgroundColor: displayCount === 10 ? 'rgba(0, 0, 0, 0.08)' : '' }} onClick={() => setDisplayCount(10)}>10</Button>
          <Button sx={{ backgroundColor: displayCount === 25 ? 'rgba(0, 0, 0, 0.08)' : '' }} onClick={() => setDisplayCount(25)}>25</Button>
          <Button sx={{ backgroundColor: displayCount === 50 ? 'rgba(0, 0, 0, 0.08)' : '' }} onClick={() => setDisplayCount(50)}>50</Button>
          <Button sx={{ backgroundColor: displayCount === 100 ? 'rgba(0, 0, 0, 0.08)' : '' }} onClick={() => setDisplayCount(100)}>100</Button>
        </ButtonGroup>
      </Box>
    </Box>
  )
}

export default PaginationSearch
