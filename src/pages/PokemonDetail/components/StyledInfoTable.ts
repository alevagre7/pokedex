import styled from '@emotion/styled'

const StyledInfoTable = styled.table`
  margin-right: 2rem;
  width: ${props => props.width};
  border: 1px solid;
  border-collapse: collapse;
  th {
    border: 1px solid;
  }
  td {
    text-align: center;
    border-right: 1px solid;
  }
`

export default StyledInfoTable
