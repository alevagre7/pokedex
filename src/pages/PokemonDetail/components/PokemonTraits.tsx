import { FunctionComponent } from 'react'
import { PokemonType } from 'pokenode-ts'
import StyledInfoTable from './StyledInfoTable'

interface PokemonTraitsProps {
  types: PokemonType[]
}

const PokemonTraits: FunctionComponent<PokemonTraitsProps> = ({ types }) => {
  return (
    <StyledInfoTable width={'100%'}>
      <tr>
        <th>TRAITS</th>
        <th>BASE</th>
      </tr>
      {types.map((type, index) => (
        <tr key={type.type.name}>
          <td>{types.length > 1 ? `TYPE ${index + 1}` : 'TYPE'}</td>
          <td>{type.type.name}</td>
        </tr>
      ))}
    </StyledInfoTable>
  )
}

export default PokemonTraits
