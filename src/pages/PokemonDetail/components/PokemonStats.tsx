import { FunctionComponent } from 'react'
import { PokemonStat } from 'pokenode-ts'
import StyledInfoTable from './StyledInfoTable'

interface PokemonStatsProps {
  stats: PokemonStat[]
}

const PokemonStats: FunctionComponent<PokemonStatsProps> = ({ stats }) => {
  return (
    <StyledInfoTable width={'50%'}>
      <tr>
        <th>STAT</th>
        <th>BASE</th>
        <th>EV</th>
      </tr>
      {stats.map((stat) => (
        <tr key={stat.stat.name}>
          <td>{stat.stat.name}</td>
          <td>{stat.base_stat}</td>
          <td>{stat.effort}</td>
        </tr>
      ))}
    </StyledInfoTable>
  )
}

export default PokemonStats
