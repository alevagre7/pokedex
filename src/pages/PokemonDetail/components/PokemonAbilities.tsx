import { FunctionComponent } from 'react'
import { PokemonAbility } from 'pokenode-ts'
import StyledInfoTable from './StyledInfoTable'

interface PokemonAbilitiesProps {
  abilities: PokemonAbility[]
}

const PokemonAbilities: FunctionComponent<PokemonAbilitiesProps> = ({ abilities }) => {
  return (
    <StyledInfoTable width={'100%'}>
      <tr>
        <th>ABILITIES</th>
      </tr>
      {abilities.map((ability, index) => (
        <tr key={ability.ability.name}>
          <td>{ability.ability.name}</td>
        </tr>
      ))}
    </StyledInfoTable>
  )
}

export default PokemonAbilities
