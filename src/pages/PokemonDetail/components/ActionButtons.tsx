import { Button, Box } from '@mui/material'
import { FunctionComponent, useEffect } from 'react'
import { useNavigate } from 'react-router-dom'
import { useAppDispatch, useAppSelector } from '../../../app/hooks'
import { fetchPokemonList, selectAllPokemonsList, selectPokemonListStatus } from '../../../features/pokemon/pokemonSlice'
export interface ActionButtonProps {
  pokemonName: string | undefined
}

// @ts-expect-error
const ActionButtons: FunctionComponent = ({ pokemonName }) => {
  const navigate = useNavigate()
  const dispatch = useAppDispatch()
  const pokemonList = useAppSelector(selectAllPokemonsList)
  const pokemonsStatus = useAppSelector(selectPokemonListStatus)

  useEffect(() => {
    if (pokemonsStatus === 'idle') {
      // @ts-expect-error
      void dispatch(fetchPokemonList())
    }
  }, [pokemonsStatus, dispatch])

  const handlePrev = (): void => {
    const prevPokemonIndex = pokemonList.findIndex(
      (pokemon, index) => pokemon?.name === pokemonName
    ) - 1
    if (prevPokemonIndex !== -1) navigate(`/pokemon/${pokemonList[prevPokemonIndex].name}/detail`)
  }

  const handleNext = (): void => {
    const nextPokemonIndex = pokemonList.findIndex(
      (pokemon, index) => pokemon?.name === pokemonName
    ) + 1
    if (nextPokemonIndex < pokemonList.length) navigate(`/pokemon/${pokemonList[nextPokemonIndex].name}/detail`)
  }

  return (
  <Box sx={{
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center'
  }}>
    <Button sx={{ fontSize: '2rem' }} onClick={handlePrev} variant="text">{'< PREV'}</Button>
    <Button sx={{ fontSize: '2rem' }} variant="text" onClick={() => navigate('/pokedex')}>BACK TO ALL</Button>
    <Button sx={{ fontSize: '2rem' }} onClick={handleNext} variant="text">{'NEXT >'}</Button>
  </Box>

  )
}

export default ActionButtons
