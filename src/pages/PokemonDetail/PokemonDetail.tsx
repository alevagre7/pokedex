import { FunctionComponent, useEffect } from 'react'
import { useParams } from 'react-router-dom'
import { Typography, Box, Container, Skeleton } from '@mui/material'
import styled from '@emotion/styled'
import { useAppDispatch, useAppSelector } from '../../app/hooks'
import { fetchPokemonByName, selectPokemonByName } from '../../features/pokemon/pokemonSlice'
import ActionButtons from './components/ActionButtons'
import PokemonStats from './components/PokemonStats'
import PokemonTraits from './components/PokemonTraits'
import PokemonAbilities from './components/PokemonAbilities'
import missingNoSprite from '../../assets/images/MissingNo.png'

const PokemonDetailContainer = styled.div`
  display: flex;
  justify-content: space-around;
  flex-direction: column;
  width: 100%;
  height: 38rem;
  border-radius: .375rem;
  background-color: #dbeafe80;
  padding-top: .5rem;
  padding-bottom: .5rem;
  cursor: pointer;
`

const PokemonArtworkContainer = styled.img`
  width: 100%;
  height: 100%;
  object-fit: contain;
`

const PokemonDetail: FunctionComponent = () => {
  const params = useParams()
  const dispatch = useAppDispatch()
  // @ts-expect-error
  const pokemonInfo = useAppSelector((state) => selectPokemonByName(state, params.pokemonName))
  const pokemonArtwork = pokemonInfo?.sprites?.other['official-artwork']?.front_default !== null &&
    pokemonInfo?.sprites?.other['official-artwork']?.front_default !== undefined
    ? pokemonInfo.sprites.other['official-artwork'].front_default
    : missingNoSprite

  useEffect(() => {
    if (pokemonInfo === undefined && params.pokemonName !== undefined) {
      // @ts-expect-error
      dispatch(fetchPokemonByName(params.pokemonName))
    }
  }, [dispatch, params.pokemonName])

  return (
    <Container>
      <Typography align="center" variant="h2" sx={{ paddingTop: '1rem' }}>
          {params.pokemonName}
      </Typography>
      {/* @ts-expect-error */}
      <ActionButtons pokemonName={params.pokemonName} />
      <Box sx={{
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
      }}>
        {pokemonInfo === undefined
          ? <Skeleton variant="rectangular" animation="wave" width={'100%'} height={'38rem'}/>
          : (
              <PokemonDetailContainer>
                <Box sx={{
                  display: 'flex',
                  justifyContent: 'center',
                  alignItems: 'center'
                }}>
                  <Box sx={{
                    width: '20rem',
                    aspectRatio: '1',
                    backgroundColor: 'rgba(118,168,242,0.5)',
                    borderRadius: '50% 50%'
                  }}>
                    <PokemonArtworkContainer src={pokemonArtwork} />
                  </Box>
                </Box>
                <Box sx={{
                  display: 'flex',
                  justifyContent: 'center',
                  alignItems: 'start',
                  flexDirection: 'row'
                }}>
                  <PokemonStats stats={pokemonInfo.stats}/>
                  <Box sx={{
                    display: 'flex',
                    flexDirection: 'column',
                    justifyContent: 'space-between',
                    height: '100%'
                  }}>
                    <PokemonTraits types={pokemonInfo.types} />
                    <PokemonAbilities abilities={pokemonInfo.abilities} />
                  </Box>
                </Box>
              </PokemonDetailContainer>
            )
        }
      </Box>
    </Container>
  )
}

export default PokemonDetail
