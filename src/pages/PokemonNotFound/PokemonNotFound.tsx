import { FunctionComponent } from 'react'
import { Container, Typography } from '@mui/material'
const PokemonNotFound: FunctionComponent = () => {
  return (
    <Container>
      <Typography align="center" variant="h2" sx={{ paddingTop: '1rem' }}>
        POKEMON NOT FOUND
      </Typography>
    </Container>
  )
}

export default PokemonNotFound
