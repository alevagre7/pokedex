import { createTheme } from '@mui/material/styles'

// A custom theme for this app
export const theme = createTheme({
  palette: {
    primary: {
      main: 'rgb(33 33 33)'
    },
    secondary: {
      main: '#ffde00'
    },
    error: {
      main: '#c21020'
    }
  },
  typography: {
    fontFamily: [
      'Bebas Neue',
      'Roboto',
      '"Helvetica"',
      'Arial',
      'sans-serif'
    ].join(',')
  }
})
