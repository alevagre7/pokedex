import { FunctionComponent } from 'react'
import { Container, Box } from '@mui/material'
import { AppRoutes } from './routes'
import pokemonBackground from './assets/images/pokemonBackground.png'
import './App.css'

export const App: FunctionComponent = () => {
  return (
        <Box sx={{
          width: '100%',
          height: '100%',
          backgroundImage: `url(${pokemonBackground})`,
          backgroundRepeat: 'repeat-y'
        }}>
          <Container sx={{
            width: '100%',
            height: '100%'
          }}>
            <AppRoutes />
          </Container>
        </Box>
  )
}
