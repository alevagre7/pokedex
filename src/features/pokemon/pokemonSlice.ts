import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import type { RootState } from '../../app/store'
import { NamedAPIResourceList, NamedAPIResource, Pokemon } from 'pokenode-ts'
import pokemonClient from '../pokemon/services/pokemonApi'
import axios from 'axios'

interface PokemonsState {
  value: {
    [pokemonName: string]: Pokemon
  }
  pokemonList: NamedAPIResource[]
  status: 'idle' | 'loading' | 'succeeded' | 'failed'
  error: string | undefined
}

const initialState: PokemonsState = {
  value: {},
  pokemonList: new Array<NamedAPIResource>(),
  status: 'idle',
  error: undefined
}

const requestApiUntilAllPokemonsAreRetrieved = async (
  response: NamedAPIResourceList, pokemonList: NamedAPIResource[]
): Promise<void> => {
  if (response.next !== null) {
    await axios.get(response.next)
      .then(async (r) => {
        pokemonList.push(...r.data.results)
        await requestApiUntilAllPokemonsAreRetrieved(r.data, pokemonList)
      })
      .catch((e) => console.log('error', e))
  }
}

export const fetchPokemonList = createAsyncThunk('pokemons/fetchPokemonList',
  async () => {
    const pokemonList: NamedAPIResource[] = new Array<NamedAPIResource>()
    const response = await pokemonClient.listPokemons(0, 100)
    pokemonList.push(...response.results)
    await requestApiUntilAllPokemonsAreRetrieved(response, pokemonList)
    return pokemonList
  })

export const fetchPokemonByName = createAsyncThunk('pokemons/fetchPokemonByUrl',
  async (pokemonName: string) => {
    const response = await pokemonClient.getPokemonByName(pokemonName)
    return response
  })

export const pokemonsSlice = createSlice({
  name: 'pokemons',
  initialState,
  reducers: {},
  extraReducers (builder) {
    builder
      .addCase(fetchPokemonList.pending, (state, _action) => {
        state.status = 'loading'
      })
      .addCase(fetchPokemonList.fulfilled, (state, action) => {
        state.status = 'succeeded'
        // Add any fetched posts to the array
        state.pokemonList = state.pokemonList.concat(action.payload)
      })
      .addCase(fetchPokemonList.rejected, (state, action) => {
        state.status = 'failed'
        state.error = action.error.message
      })
      .addCase(fetchPokemonByName.fulfilled, (state, action) => {
        state.value[action.payload.name] = action.payload
      })
  }
})

export const selectPokemonByName = (state: RootState, pokemonName: Pokemon['name']): Pokemon => state.pokemons.value[pokemonName]
export const selectPokemonListStatus = (state: RootState): PokemonsState['status'] => state.pokemons.status
export const selectAllPokemonsList = (state: RootState): NamedAPIResource[] => state.pokemons.pokemonList

export default pokemonsSlice.reducer
