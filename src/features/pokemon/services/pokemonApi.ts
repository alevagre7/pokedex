// Original api documentation
// https://pokeapi.co/docs/v2#pokemon

// ts wrapper
// https://github.com/Gabb-c/pokenode-ts
import { NamedAPIResourceList, PokemonClient } from 'pokenode-ts'

const api = new PokemonClient()

export default api
